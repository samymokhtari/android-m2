package com.samy.cours

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.samy.cours.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnRecyclerview.setOnClickListener { goRecyclerViewActivity() }
    }

    private fun goRecyclerViewActivity() {
        startActivity(Intent(this, RecyclerViewActivity::class.java))
    }
}