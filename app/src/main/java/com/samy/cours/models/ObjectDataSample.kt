package com.samy.cours.models

data class ObjectDataSample(
    val versionName : String,
    val versionCode : Int
)